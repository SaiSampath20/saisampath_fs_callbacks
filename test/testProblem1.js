const createDirectoryAndDeletefilesInDirectory = require("../problem1");

// while callback is passed
try {
  createDirectoryAndDeletefilesInDirectory((err, message) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log(message);
    }
  });
} catch (error) {
  console.log(error.message);
}

// while callback is not passed
try {
  createDirectoryAndDeletefilesInDirectory();
} catch (error) {
  console.log(error.message);
}
