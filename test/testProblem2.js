const readAndConvert = require("../problem2");

// while callback is passed
try {
  readAndConvert((err, message) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log(message);
    }
  });
} catch (error) {
  console.log(error.message);
}

// while callback is not passed
try {
  readAndConvert();
} catch (error) {
  console.log(error.message);
}
