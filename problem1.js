const fs = require("fs");

function writeFileIntoDirectory(callback, cb) {
  let data = "some";

  for (let index = 0; index < 3; index++) {
    fs.writeFile(`./randomFiles/demo${index}.json`, data, (err1) => {
      try {
        if (err1) {
          throw new Error("cannot write files");
        } else {
          cb(null, "files written ");
        }
        if (index === 2) {
          callback(cb);
        }
      } catch (error) {
        cb(error);
      }
    });
  }
}

function deleteFileIntoDirectory(cb) {
  for (let index = 0; index < 3; index++) {
    fs.unlink(`./randomFiles/demo${index}.json`, (err2) => {
      try {
        if (err2) {
          throw new Error("cannot delete files");
        } else {
          cb(null, `file deleted ${index}`);
        }
      } catch (error) {
        cb(error);
      }
    });
  }
}

function createDirectoryAndDeletefilesInDirectory(cb) {
  if (typeof cb !== "function") {
    throw new Error("Invalid arguments");
  }

  try {
    fs.mkdir("./randomFiles", { recursive: true }, (err) => {
      if (err) {
        throw new Error("error while creating directory");
      } else {
        writeFileIntoDirectory(deleteFileIntoDirectory, cb);
      }
    });
  } catch (error) {
    cb(error);
  }
}

module.exports = createDirectoryAndDeletefilesInDirectory;
