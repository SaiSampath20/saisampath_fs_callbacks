const fs = require("fs");

function readFile(filePath, callback) {
  fs.readFile(filePath, "utf-8", (err, data) => {
    if (err) {
      callback(err);
    } else {
      callback(null, data);
    }
  });
}

function writeFile(filePath, data, flag, callback) {
  if (flag === undefined) {
    fs.writeFile(filePath, data, (err) => {
      if (err) {
        callback(err);
      } else {
        callback();
      }
    });
  } else {
    fs.writeFile(filePath, data, flag, (err) => {
      if (err) {
        callback(err);
      } else {
        callback();
      }
    });
  }
}

function readAndConvert(callback) {
  if (typeof callback !== "function") {
    throw new Error("Invalid arguments");
  }

  try {
    readFile("./lipsum.txt", (err, data) => {
      if (err) throw new Error("problem in reading file");

      writeFile("./lipsumUpperCase.txt", data.toUpperCase(), null, (err) => {
        if (err) throw new Error("problem in writing file");

        writeFile("./filenames.txt", "lipsumUpperCase.txt", null, (err) => {
          if (err) throw new Error("problem in writing file");

          readFile("./lipsumUpperCase.txt", (err, data) => {
            if (err) throw new Error("problem in reading file");

            writeFile(
              "./lipsumLowerCase.txt",
              JSON.stringify(data.toLowerCase().split(".")),
              null,
              (err) => {
                if (err) throw new Error("problem in writing file");

                writeFile(
                  "./filenames.txt",
                  " lipsumLowerCase.txt",
                  { encoding: "utf-8", flag: "a" },
                  (err) => {
                    if (err) throw new Error("problem in writing file");

                    readFile("./lipsumLowerCase.txt", (err, data) => {
                      if (err) throw new Error("problem in reading file");

                      let myArray = JSON.parse(data);

                      myArray.sort();

                      writeFile(
                        "./sortedlipsum.txt",
                        JSON.stringify(myArray),
                        null,
                        (err) => {
                          if (err) throw new Error("problem in writing file");

                          writeFile(
                            "./filenames.txt",
                            " sortedlipsum.txt",
                            { encoding: "utf-8", flag: "a" },
                            (err) => {
                              if (err) throw new Error("problem in writing file");

                              readFile("./filenames.txt", (err, data) => {
                                if (err) throw new Error("problem in reading file");

                                let fileArr = data.split(" ");

                                fileArr.forEach((file) => {
                                  fs.unlink(file, (err) => {
                                    if (err)
                                      throw new Error(
                                        "problem in deleting file"
                                      );
                                  });
                                });

                                callback(null, "deleted successfully");
                              });
                            }
                          );
                        }
                      );
                    });
                  }
                );
              }
            );
          });
        });
      });
    });
  } catch (error) {
    callback(error);
  }
}

module.exports = readAndConvert;
